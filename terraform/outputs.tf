// Show servers internal IPs
output "Internal_IP_of_first_instance" {
  value = "${yandex_compute_instance.instance1.network_interface.0.ip_address}"
}
output "Internal_IP_of_second_instance" {
  value = "${yandex_compute_instance.instance2.network_interface.0.ip_address}"
}



// Show servers external IPs
output "External_IP_of_first_instance" {
  value = "${yandex_compute_instance.instance1.network_interface.0.nat_ip_address}"
}
output "External_IP_of_second_instance" {
  value = "${yandex_compute_instance.instance2.network_interface.0.nat_ip_address}"
}